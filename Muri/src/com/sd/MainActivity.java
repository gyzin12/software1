/*
 * < 제일 먼저 보여지는 화면 >
 * button_s 선택시 , search 화면으로 넘어간다. ( SelectCityActivity2 )
 * button_r 선택시, register 화면으로 넘어간다. ( SelectCityActivity )
 */

package com.sd;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        startActivity(new Intent(this, MainSplash.class));
        /*search button 눌렀을 때*/
        Button b1 = (Button)findViewById(R.id.button_s);
        b1.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		Intent intent_search = new Intent(getApplicationContext(), SelectCityActivity2.class);
        		startActivity(intent_search);
        	}
        });
        /*register button 눌렀을때 - 지도 등록*/
        Button b2 = (Button)findViewById(R.id.button_r);
        b2.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		Intent intent_regi = new Intent(getApplicationContext(), SelectCityActivity.class); // 여기를 고치기 !!!
        		startActivity(intent_regi);
        	}
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
