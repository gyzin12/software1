package com.sd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.http.client.entity.UrlEncodedFormEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.annotation.SuppressLint;
import android.content.Intent;

/*
 * db 에 등록하는 activity 
 * price hairstyle shopname gender
 * RadioGroup - radio ( radiobt_female, radiobt_male )
 */
@SuppressLint("NewApi") 
public class RegisterActivity extends Activity{
	
	EditText price_text = null;
	EditText hairstyle_text = null;
	EditText shopname_text = null;
	RadioGroup radio;
	String gender;
	
	TextView tv = null;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registerdb);
        
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        
        
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);}
        
        price_text = (EditText)findViewById(R.id.price3);
        hairstyle_text = (EditText)findViewById(R.id.hairstyle3);
        shopname_text = (EditText)findViewById(R.id.shopname3);

        
        radio = (RadioGroup)findViewById(R.id.radio3);
        
        shopname_text.setText(bundle.getString("shopnamekey"));
        hairstyle_text.setText(bundle.getString("locationkey"));
        
     // register_bt 등록버튼을 클릭했을때 실행되도록 
        Button b1 = (Button)findViewById(R.id.register_bt);
        b1.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		             		
        		if ( price_text.getText().toString().equals("") && hairstyle_text.getText().toString().equals("")){
        			Toast.makeText(getApplicationContext(), "입력해주세요!", Toast.LENGTH_SHORT).show();
        			return;
        		}
        		
        		String hairstyle = hairstyle_text.getText().toString();
        		String shopname = shopname_text.getText().toString();
        		String price = price_text.getText().toString();
        		
        		
        		if(radio.getCheckedRadioButtonId()==R.id.radiobt_female3)
            		gender = "여자";
            	if(radio.getCheckedRadioButtonId()==R.id.radiobt_male3)
            		gender = "남자";
            	
        		Toast.makeText(getApplicationContext(), "등록되었습니당:)", Toast.LENGTH_SHORT).show();
        		
        		StringBuilder builder = new StringBuilder();
        		
                try {            
                	URL url = new URL("http://uranus.smu.ac.kr/~201111251/registerdb.php?"+"hairstyle="+URLEncoder.encode(hairstyle, "UTF-8")
        					+"&price="+URLEncoder.encode(price, "UTF-8")
                			+"&shopname="+URLEncoder.encode(shopname, "UTF-8")
        					+"&gender="+URLEncoder.encode(gender, "UTF-8"));
                	
        			HttpURLConnection conn = (HttpURLConnection)url.openConnection();

        			if(conn != null){
        				conn.setConnectTimeout(1000);
        				conn.setUseCaches(false);    				
        				
        				if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){	
        					
        					BufferedReader read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        					
    						
        					while(true){
        						String line = read.readLine();
        						if(line == null)
        						{
        							break;
        						}
        						builder.append(line+"\n");		
        					}
        					read.close();
        					conn.disconnect();
        				}
        			}
        		} 
                catch (Exception e) {
                	tv.setText(e.toString());
        		} 
        	}
        });
   }
	
	 @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu.main, menu);
	        return true;
	    }       
}