/*
 * �ε� ȭ��
 */

package com.sd;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class MainSplash extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainsplash);
		
		Handler hd = new Handler(){
			@Override
			public void handleMessage(Message msg){
				finish();
			}
		};
		hd.sendEmptyMessageDelayed(0,3000);
	}

}
