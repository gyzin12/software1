package com.sd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.widget.Toast;

@SuppressLint("NewApi") 
public class SearchMap extends FragmentActivity {
	
	private GoogleMap map;
	private Marker mar;
	String region = null;
	String tempr = null;
	String shopname = null;
	int price;
	int price2;
	String gender = null;
	String hairstyle = null;
	LatLng bigregion ;
	String shopname2 = null;
	String message = null;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        
        region = bundle.getString("regionkey");
        bigregion =getlatlng(region);
        price = bundle.getInt("pricekey");
        gender = bundle.getString("genderkey");
        hairstyle = bundle.getString("hairstylekey");
        
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);}
        
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        Toast.makeText(getApplicationContext(), region  , Toast.LENGTH_LONG).show();
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(bigregion, 15));
        showhairshop();
        
       
        
	}
	
	private void showhairshop(){
    	 try {
	    		
              	URL url = new URL("http://uranus.smu.ac.kr/~201111251/searchlocationdb.php?" + "&location=" + URLEncoder.encode(region, "UTF-8") ) ;//;
      			HttpURLConnection conn = (HttpURLConnection)url.openConnection();

      			
      			if(conn != null){
      				conn.setConnectTimeout(1000);
      				conn.setUseCaches(false);
      				
      				
      				if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){	
     
      					StringBuilder builder = new StringBuilder();
      					BufferedReader read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      			  		while(true){
      						String line = read.readLine();
      						if(line == null)
      						{
      							break;
      						}
      						builder.append(line+"\n");		

      					}
      					 String jsonString = builder.toString();
      		                
      		                try {
      		                        JSONArray ja = new JSONArray(jsonString);
      		  
      		                        for (int i = 0; i < ja.length(); i++) {
      		                               JSONObject jo = ja.getJSONObject(i);
      		                               //결과물
      		                               tempr = jo.getString("location");
      		                               shopname = jo.getString("shopname");
      		                               searchhairshopdb(shopname);
      		                               
      		                             
      		                        }
      		  
      		                } catch (JSONException e) {
      		                        // TODO Auto-generated catch block
      		                }
      			
      					conn.disconnect();
      
      				}
      			}
      		} 
              catch (Exception e) {
              	
      		}      
    }
	public void showLocation(String shop, String locname, int pri)   //위치를 한글 주소로 변환해 주는 함수
	{
		LatLng add = getlatlng(locname);
		shop = shop + "\nprice: " +pri;
		if(pri <= price/4)
			mar = map.addMarker(new MarkerOptions().position(add).title(shop).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
		else if(pri <= (price*2/4))
			mar = map.addMarker(new MarkerOptions().position(add).title(shop).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
		else if(pri <= (price*3/4))
			mar = map.addMarker(new MarkerOptions().position(add).title(shop).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
		else if(pri <= (price*4/4))
			mar = map.addMarker(new MarkerOptions().position(add).title(shop).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
		
		}
	public void searchhairshopdb(String shop){
		 StringBuilder builder = new StringBuilder();	
		    try {            
		    	URL url = new URL("http://uranus.smu.ac.kr/~201111251/searchdb.php?"+"hairstyle="+URLEncoder.encode(hairstyle, "UTF-8")
						+"&price="+ price 
		    			+"&shopname="+URLEncoder.encode(shop, "UTF-8")
						+"&gender="+URLEncoder.encode(gender, "UTF-8"));
		    	
				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
					
				//tv.setText(String.valueOf(conn.getResponseCode())+"\n"+String.valueOf(HttpURLConnection.HTTP_OK));
				if(conn != null){
					conn.setConnectTimeout(1000);
					conn.setUseCaches(false);    				
					
					if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){	
						
						BufferedReader read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						
						
						while(true){
							String line = read.readLine();
							if(line == null)
							{
								break;
							}
							builder.append(line+"\n");		
						}
						read.close();
						conn.disconnect();
					}
				}
			} 
		    catch (Exception e) {
		    	//tv.setText(e.toString());
			}      
			
		   String jsonString = builder.toString();
		    
		    try {
		            String res = "";
		            JSONArray ja = new JSONArray(jsonString);

		            for (int i = 0; i < ja.length(); i++) {
		                   JSONObject jo = ja.getJSONObject(i);
		                   shopname2 = jo.getString("shopname");
		                   price2 = jo.getInt("price");
		                   showLocation(shop, tempr, price2);
		                   //결과물
		                   /*res += "hairstyle : " + jo.getString("hairstyle") 
		                	   + ", price : " + jo.getString("price") 
		                	   + ", gender : "+ jo.getString("gender")
		                       + ", shopname : "+ jo.getString("shopname")+ "\n";*/
		                  // res += "shopname : " + jo.getString("shopname")+"\n";
		            }
		            //결과 출력
		         //   tv.setText(res);

		    } catch (JSONException e) {
		            // TODO Auto-generated catch block
		          //  tv.setText(e.toString());
		    }
	}
	public LatLng getlatlng(String locname){
		Geocoder gcK = new Geocoder(this, Locale.KOREA);            //Geocoder로 한글 주소로 바꾼다.

		LatLng add = new LatLng(0, 0);
		try {

			List<Address> address= gcK.getFromLocationName(locname, 60);

			double latt = address.get(0).getLatitude() ;
			double lngg = address.get(0).getLongitude();
			
			add = new LatLng(latt, lngg);
			return add;

		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return add;
	}


	public String getAddress(LatLng lng)   //위치를 한글 주소로 변환해 주는 함수
	{
		Geocoder gcK = new Geocoder(this, Locale.KOREA);            //Geocoder로 한글 주소로 바꾼다.
		String res = "null";
		try {
			List<Address> addresses = gcK.getFromLocation(lng.latitude, lng.longitude, 1);//변환한 한글주소를 배열로 저장
			StringBuilder sb = new StringBuilder();
			Address address = addresses.get(0); 
			sb.append(address.getLocality()).append(" ");      //지역이름
			sb.append(address.getThoroughfare()).append(" ");  //동이름
			sb.append(address.getFeatureName()).append(" ");    //번지수 
			res = sb.toString(); //문자열로 변환
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	

	

}
