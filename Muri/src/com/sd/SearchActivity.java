package com.sd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.http.client.entity.UrlEncodedFormEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.annotation.SuppressLint;
import android.content.Intent;

/*
 * db 에서 검색해서 값을 가져오는 activity 
 * price hairstyle  
 * RadioGroup - radio ( radiobt_female, radiobt_male )
 */
@SuppressLint("NewApi") 
public class SearchActivity extends Activity{
	
	EditText price_text = null;
	EditText hairstyle_text = null;
	//EditText shopname_text = null;
	RadioGroup radio;
	String gender;
	String region;
	
	TextView tv = null;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchhair);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        
        region = bundle.getString("citykey") + " " + bundle.getString("gukey") + " " + bundle.getString("dongkey");
        
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);}
        
        price_text = (EditText)findViewById(R.id.price2);
        hairstyle_text = (EditText)findViewById(R.id.hairstyle2);
        
        radio = (RadioGroup)findViewById(R.id.radio2);
        
     // register_bt 등록버튼을 클릭했을때 실행되도록 
        Button b1 = (Button)findViewById(R.id.search_bt);
        b1.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){

        		if ( price_text.getText().toString().equals("") && hairstyle_text.getText().toString().equals("")){
        			Toast.makeText(getApplicationContext(), "입력해주세요!", Toast.LENGTH_SHORT).show();
        			return;
        		}
        		
        		String hairstyle = hairstyle_text.getText().toString();
        		String shopname = "aaa";
        		int price = Integer.parseInt(price_text.getText().toString());
        		
        		
        		if(radio.getCheckedRadioButtonId()==R.id.radiobt_female2)
            		gender = "여자";
            	if(radio.getCheckedRadioButtonId()==R.id.radiobt_male2)
            		gender = "남자";      
            	
            	Intent intent_searchm =  new Intent(getApplicationContext(), SearchMap.class);
            	Bundle bundle = new Bundle();
            	bundle.putString("regionkey", region);
            	bundle.putString("hairstylekey", hairstyle);
            	bundle.putInt("pricekey", price);
            	bundle.putString("genderkey", gender);
            	intent_searchm.putExtras(bundle);
            	startActivity(intent_searchm);
     
        	}
        });
   }
	
	 @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu.main, menu);
	        return true;
	    }       
}