/*
 * 
 * 도시를 먼저 선택하는 activity
 * 그 다음 구를 선택하는 SelectGuActivity2로 넘어간다
 */

package com.sd;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SelectCityActivity extends Activity{

	String city;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.area);
			 Button bs = (Button)findViewById(R.id.button_seoul);
		     bs.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "서울시";
		     		Intent intent = new Intent(getApplicationContext(), SelectGuActivity.class);
		     		Bundle cdata = new Bundle();
					cdata.putString("citykey", city);
					intent.putExtras(cdata);
		     		startActivity(intent);
		     	}
		     });
		     Button kw = (Button)findViewById(R.id.button_kw);
		     kw.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "강원도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });
		     Button kg = (Button)findViewById(R.id.button_kg);
		     kg.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "경기도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });
		     Button cb = (Button)findViewById(R.id.button_cb);
		     cb.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "충청북도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });
		     Button chn = (Button)findViewById(R.id.button_chn);
		     chn.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "충청남도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });
		     Button jn = (Button)findViewById(R.id.button_jn);
		     jn.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "전라남도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });
		     Button jb = (Button)findViewById(R.id.button_jb);
		     jb.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "전라북도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });
		     Button kn = (Button)findViewById(R.id.button_kn);
		     kn.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "경상남도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });
		     Button kb = (Button)findViewById(R.id.button_kb);
		     kb.setOnClickListener(new OnClickListener(){
		     	public void onClick(View v){
		     		city = "경상북도";
		     		Toast.makeText(getApplicationContext(), city  , Toast.LENGTH_LONG).show();
		     	}
		     });		     
		     
	}
	
	
}
