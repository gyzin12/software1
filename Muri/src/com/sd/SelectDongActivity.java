/*
 * 동을 선택하는 activity
 * 그 다음 해당 지역의 지도화면으로 넘어간다.
 */

package com.sd;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SelectDongActivity extends Activity{

	String region = null;
	String city = null;	
	String gu = null;	
	
	static final String[] dong_mapo = { "망원동","서교동","아현동","공덕동","상수동","마포동","합정동","서강동","중동","염리동" };
	static final String[] dong_gangnam = { "신사동", "논현동", "압구정동","대치동","삼성동","역삼동","수서동","도곡동","개포동","세곡동","수서동" };
	static final String[] dong_gangseo = { "염창동", "등촌동", "화곡동", "우장산동" , "가양동", "발산동" ,"공항동" , "방화동" };
	
	private ArrayAdapter<String> dongAdapter;
	private ListView list;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_dong);	
		
		Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        city = bundle.getString("citykey");
        gu = bundle.getString("gukey");
        
        if(gu.equals("마포구")){
        	dongAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_single_choice,dong_mapo);
        }
        if(gu.equals("강남구")){
    		dongAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_single_choice,dong_gangnam);
        }
        if(gu.equals("강서구")){
    		dongAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_single_choice,dong_gangseo);
        }
        
		list = (ListView)findViewById(R.id.listView3);
		list.setAdapter(dongAdapter);
  
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id){
				String dong_s = (String)list.getItemAtPosition(position);
					Intent intent = new Intent(getApplicationContext(), ShowMap.class);
					Bundle gdata = new Bundle();
					gdata.putString("gukey", gu);
					gdata.putString("citykey", city);
					gdata.putString("dongkey", dong_s);
					intent.putExtras(gdata);
	        		startActivity(intent);
			}
		});   
        	
    }
	
	
}
