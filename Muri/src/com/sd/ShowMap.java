package com.sd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.SupportMapFragment;


@SuppressLint("NewApi") 
public class ShowMap extends FragmentActivity {
	
	private GoogleMap map;
	private LatLng loc;
	private Marker m;
	private MarkerOptions marker;
	private Marker mar;
	String region = null;
	String tempr = null;
	String shopname = null;
	LatLng bigregion ;


	//읽어올때, double latitude[] double longitude배열로 받아온 후 저장하고 for문을 돌려서 출력.

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.map);
	        
	        Intent intent = getIntent();
	        Bundle bundle = intent.getExtras();
	        
	        
	        if (android.os.Build.VERSION.SDK_INT > 9) {
	            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	            StrictMode.setThreadPolicy(policy);}
	        
	        
	        //startdbread();
	        
	        region = bundle.getString("citykey")  +" "+ bundle.getString("gukey")  + " " +bundle.getString("dongkey") ;
	        //+ gu_text.getText().toString() + dong_text.getText().toString();
	        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
	        bigregion =getlatlng(region);
	        map.animateCamera(CameraUpdateFactory.newLatLngZoom(bigregion, 15));
	        showhairshop();
	        // 지도 객체 참조
	        
	        
	        //LatLng camloc = new LatLng(x, y);
			//LatLng camloc = new LatLng(37.558731,126.978779);
			//map.animateCamera(CameraUpdateFactory.newLatLngZoom(camloc, 15));
			//showCertainLocation(x,y);
			//showCertainLocation(37.558731,126.978779);
			//showCertainLocation(37.45678,126.56784);
	        //showLocation("서울시 마포구 망원동");
	        //showLocation(a);
	        
	        Toast.makeText(getApplicationContext(), region  , Toast.LENGTH_LONG).show();
	        map.setOnInfoWindowClickListener(new OnInfoWindowClickListener(){

				@Override
				public void onInfoWindowClick(Marker marker) {
					// TODO Auto-generated method stub
					Intent intent_regi = new Intent(getApplicationContext(), RegisterActivity.class); // 여기를 고치기 !!!
	        		Bundle bundle2 = new Bundle();
	        		bundle2.putString("shopnamekey", marker.getTitle());
	        		tempr = getAddress(marker.getPosition());
	        		bundle2.putString("locationkey", tempr);
	        		intent_regi.putExtras(bundle2);
					startActivity(intent_regi);
					
				}
	        	
	        });


	        // 위치 확인하여 위치 표시 시작
	       // startLocationService();
	    }
	    private void showhairshop(){
	    	 try {
		    		
	              	URL url = new URL("http://uranus.smu.ac.kr/~201111251/searchlocationdb.php?" + "&location=" + URLEncoder.encode(region, "UTF-8") ) ;//;
	      			HttpURLConnection conn = (HttpURLConnection)url.openConnection();

	      			
	      			if(conn != null){
	      				conn.setConnectTimeout(1000);
	      				conn.setUseCaches(false);
	      				
	      				
	      				if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){	
	     
	      					StringBuilder builder = new StringBuilder();
	      					BufferedReader read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	      			  		while(true){
	      						String line = read.readLine();
	      						if(line == null)
	      						{
	      							break;
	      						}
	      						builder.append(line+"\n");		

	      					}
	      					 String jsonString = builder.toString();
	      		                
	      		                try {
	      		                        JSONArray ja = new JSONArray(jsonString);
	      		  
	      		                        for (int i = 0; i < ja.length(); i++) {
	      		                               JSONObject jo = ja.getJSONObject(i);
	      		                               //결과물
	      		                               tempr = jo.getString("location");
	      		                               shopname = jo.getString("shopname");
	      		                               showLocation(tempr);
	      		                             
	      		                        }
	      		  
	      		                } catch (JSONException e) {
	      		                        // TODO Auto-generated catch block
	      		                }
	      			
	      					conn.disconnect();
	      
	      				}
	      			}
	      		} 
	              catch (Exception e) {
	              	
	      		}      
	    	 
	    	 
	    	
	    
	    }
	 
	    private void startdbread(){
	    	
	    	  try {
	    		
              	URL url = new URL("http://uranus.smu.ac.kr/~201111251/locationdb.php?" + "&shopname=" + URLEncoder.encode("서울시", "UTF-8") + "&location=" + URLEncoder.encode("서울시", "UTF-8") ) ;//;
      			HttpURLConnection conn = (HttpURLConnection)url.openConnection();

      			
      			if(conn != null){
      				conn.setConnectTimeout(1000);
      				conn.setUseCaches(false);
      				
      				
      				if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){	
     
      					StringBuilder builder = new StringBuilder();
      					BufferedReader read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      			  		while(true){
      						String line = read.readLine();
      						if(line == null)
      						{
      							break;
      						}
      						builder.append(line+"\n");		

      					}
      					 String jsonString = builder.toString();
      		                
      		                try {
      		                        JSONArray ja = new JSONArray(jsonString);
      		  
      		                        for (int i = 0; i < ja.length(); i++) {
      		                               JSONObject jo = ja.getJSONObject(i);
      		                               //결과물
      		                               double x = jo.getDouble("locationx");
      		                               double y = jo.getDouble("locationy");
      		                               
      		                        }
      		  
      		                } catch (JSONException e) {
      		                        // TODO Auto-generated catch block
      		                }
      			
      					conn.disconnect();
      
      				}
      			}
      		} 
              catch (Exception e) {
              	
      		}      
          
        
	    
	    }
	    /**
	     * 현재 위치 확인을 위해 정의한 메소드
	     */
	    private void startLocationService() {
	    	// 위치 관리자 객체 참조
	    	LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

			// 리스너 객체 생성
	    	GPSListener gpsListener = new GPSListener();
			long minTime = 10000;
			float minDistance = 0;

			// GPS 기반 위치 요청

			manager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER,
						minTime,
						minDistance,
						gpsListener);
			
			// 네트워크 기반 위치 요청
			manager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER,
					minTime,
					minDistance,
					gpsListener);

			Toast.makeText(getApplicationContext(), "위치 확인 시작함. 로그를 확인하세요.", Toast.LENGTH_SHORT).show();
	    }

	    /**
	     * 리스너 정의
	     */
		private class GPSListener implements LocationListener {
			/**
			 * 위치 정보가 확인되었을 때 호출되는 메소드
			 */
		    public void onLocationChanged(Location location) {
				Double latitude = location.getLatitude();
				Double longitude = location.getLongitude();

				String msg = "Latitude : "+ latitude + "\nLongitude:"+ longitude;
				Log.i("GPSLocationService", msg);
				loc = new LatLng(latitude, longitude);
				// 현재 위치의 지도를 보여주기 위해 정의한 메소드 호출
				showCurrentLocation(latitude, longitude);

			}

		    public void onProviderDisabled(String provider) {
		    }

		    public void onProviderEnabled(String provider) {
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {
		    }

		}

		/**
		 * 현재 위치의 지도를 보여주기 위해 정의한 메소드
		 * 
		 * @param latitude
		 * @param longitude
		 */
		private void showCurrentLocation(Double latitude, Double longitude) {
			// 현재 위치를 이용해 LatLon 객체 생성
			LatLng curPoint = new LatLng(latitude, longitude);
			double x = latitude;
			double y = longitude;
			map.clear();

			map.animateCamera(CameraUpdateFactory.newLatLngZoom(curPoint, 15));

			// 지도 유형 설정. 지형도인 경우에는 GoogleMap.MAP_TYPE_TERRAIN, 위성 지도인 경우에는 GoogleMap.MAP_TYPE_SATELLITE
			map.setMapType(GoogleMap.MAP_TYPE_NORMAL); 
			marker = new MarkerOptions().position(loc).title(curPoint.toString());
			//marker.s
			m = map.addMarker(marker);
			
			map.setOnMarkerClickListener(new OnMarkerClickListener(){

				@Override
				public boolean onMarkerClick(Marker arg0) {
					// TODO Auto-generated method stub
			
					startdbread();
					Toast.makeText(getApplicationContext(), "등록"  , Toast.LENGTH_LONG).show();
					return false;
				}

			});
			
		}
		private void showCertainLocation(Double latitude, Double longitude) {
			
				LatLng pune = new LatLng(latitude, longitude);
				
				String title = latitude +longitude +"";
				Marker mar = map.addMarker(new MarkerOptions().position(pune).title(title).icon(BitmapDescriptorFactory.defaultMarker(36)));
				//marker색 36 marker 이미지로 넣을때 BitmapDescriptorFactory.fromResource(R.drawable.arrow)
		}
		
		public void showLocation(String locname)   //위치를 한글 주소로 변환해 주는 함수
		{
			    LatLng add = getlatlng(locname);
				mar = map.addMarker(new MarkerOptions().position(add).title(shopname).icon(BitmapDescriptorFactory.defaultMarker(23)));
			
		}
		public LatLng getlatlng(String locname){
			Geocoder gcK = new Geocoder(this, Locale.KOREA);            //Geocoder로 한글 주소로 바꾼다.
			//String res = "null";
			LatLng add = new LatLng(0, 0);
			try {
				//locname = "서울시 강남구 공덕동 ";
				List<Address> address= gcK.getFromLocationName(locname, 60);
						//gcK.getFromLocation(lat, lng, 1);//변환한 한글주소를 배열로 저장
				//StringBuilder sb = new StringBuilder();
				double latt = address.get(0).getLatitude() ;
				double lngg = address.get(0).getLongitude();
				
				add = new LatLng(latt, lngg);
				return add;

			/*	Address address = addresses.get(0);
				//나라이름
				sb.append(address.getCity()).append(" "); 
				address.get(0)//지역이름
				sb.append(address.getgetThoroughfare()).append(" ");  //동이름
				sb.append(address.getFeatureName()).append(" ");    //번지수 
				res = sb.toString(); //문자열로 변환*/
			} 
			catch (IOException e)
			{
				e.printStackTrace();
			}
			return add;
		}


		public String getAddress(LatLng lng)   //위치를 한글 주소로 변환해 주는 함수
		{
			Geocoder gcK = new Geocoder(this, Locale.KOREA);            //Geocoder로 한글 주소로 바꾼다.
			String res = "null";
			try {
				List<Address> addresses = gcK.getFromLocation(lng.latitude, lng.longitude, 1);//변환한 한글주소를 배열로 저장
				StringBuilder sb = new StringBuilder();
				Address address = addresses.get(0); 
				sb.append(address.getLocality()).append(" ");      //지역이름
				sb.append(address.getThoroughfare()).append(" ");  //동이름
				sb.append(address.getFeatureName()).append(" ");    //번지수 
				res = sb.toString(); //문자열로 변환
			} 
			catch (IOException e)
			{
				e.printStackTrace();
			}
			return res;
		}
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}





}
