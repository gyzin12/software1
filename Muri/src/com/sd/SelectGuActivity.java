/*
 * 구를 선택하는 activity
 * 그 다음 동을 선택하는 activity로 넘어간다.
 */

package com.sd;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SelectGuActivity extends Activity{
	
	String region = null;
	String city = null;		

	static final String[] gu = { "강남구","마포구","강서구","광진구","구로구","금천구","노원구","도봉구","동작구","서대문구","서초구","성동구","은평구" };
	String a;
	
	private ArrayAdapter<String> guAdapter;
	private ListView list;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_gu);
		
		Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        city = bundle.getString("citykey");
		
		guAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_single_choice,gu);
		
		list = (ListView)findViewById(R.id.listView2);
		list.setAdapter(guAdapter);

		//list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);     
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id){
				String gu_s = (String)list.getItemAtPosition(position);
					Intent intent = new Intent(getApplicationContext(), SelectDongActivity.class); // 여기를 고치기 !!!
					Bundle gdata = new Bundle();
					gdata.putString("gukey", gu_s);
					gdata.putString("citykey", city);
					intent.putExtras(gdata);
	        		startActivity(intent);				
			}
		});        	
    }	
	
}
